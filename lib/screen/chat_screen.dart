import 'package:chat_app/widget/chat/chat_input_widget.dart';
import 'package:chat_app/widget/chat/messages_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({super.key});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat Screen'),
        backgroundColor: const Color(0xffee9ca7),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: DropdownButton(
              underline: Container(),
              icon: const Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
              items: [
                DropdownMenuItem(
                  value: 'logout',
                  child: Row(
                    children: const [
                      Icon(
                        Icons.exit_to_app,
                        color: Colors.black,
                      ),
                      SizedBox(width: 8),
                      Text('Logout')
                    ],
                  ),
                ),
              ],
             onChanged: (itemIdentifier) {
               if (itemIdentifier == 'logout') {
                 FirebaseAuth.instance.signOut();
               }
             },
            ),
          )
        ],
      ),
      body: Center(
        child: Column(
          children: const [
            Expanded(
              child: MessagesWidget(),
            ),
            ChatInputWidget()
          ],
        )
      )
    );
  }
}
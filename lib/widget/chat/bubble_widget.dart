import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BubbleWidget extends StatelessWidget {

  final String message;
  final String userName;
  final String userImage;
  final Timestamp createdAt;
  final bool isMe;

  const BubbleWidget({
    Key? key,
    required this.message,
    required this.userName,
    required this.userImage,
    required this.createdAt,
    required this.isMe,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Row(
          mainAxisAlignment: isMe == true ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: isMe == true ? CrossAxisAlignment.end : CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: isMe == true ? const Color(0xffffdde1) : const Color(0xffee9ca7),
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(12),
                      topRight: const Radius.circular(12),
                      bottomLeft: !isMe ? const Radius.circular(0) : const Radius.circular(12),
                      bottomRight: isMe ? const Radius.circular(0) : const Radius.circular(12),
                    ),
                  ),
                  width: 140,
                  padding: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 16,
                  ),
                  margin: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 8,
                  ),
                  child: Column(
                    crossAxisAlignment: isMe == true ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                    children: [
                      Text(
                        userName,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: isMe == true ? Colors.black : Colors.white,
                        ),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        message,
                        style: TextStyle(
                          color: isMe == true ? Colors.black : Colors.white,
                        ),
                        textAlign: isMe == true ? TextAlign.end : TextAlign.start,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: isMe == true 
                    ? const EdgeInsets.only(right: 8) 
                    : const EdgeInsets.only(left : 8),
                  child: Text(
                    DateFormat('dd MMM yyyy, kk:mm').format(createdAt.toDate()).toString(),
                    textAlign: isMe == true ? TextAlign.end : TextAlign.start,
                    style: const TextStyle(
                      color: Colors.grey,
                      fontSize: 12,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        Positioned(
          top: 0,
          left: isMe == true ? null : 120,
          right: isMe == true ? 120 : null,
          child: CircleAvatar(
            backgroundImage: NetworkImage(userImage),
          )
        )
      ],
    );
  }
}